package com.logica.seguridad;

import java.util.ArrayList;

import com.datos.comun.SEG_ProveedoresDL;
import com.entidades.comun.CMM_ProveedoresEL;

public class CMM_ProveedoresBL {
	public static CMM_ProveedoresBL _Instancia;
	private CMM_ProveedoresBL(){};
	public static CMM_ProveedoresBL Instancia(){
		if(_Instancia==null){
			_Instancia = new CMM_ProveedoresBL();
		}
		return _Instancia;
	}
	public ArrayList<CMM_ProveedoresEL> ListarProveedores() throws Exception {
		try {
			return SEG_ProveedoresDL.Instancia().ListarProveedores();
		} catch (Exception e) {
			throw e;
		}
		
	}
	// Agregar Proveedores
		public boolean AgregarProveedores(CMM_ProveedoresEL proveedores) throws Exception {
			boolean var = false;
			try {
				var = SEG_ProveedoresDL.Instancia().AgregarProveedores(proveedores);
				if(var){
					System.out.println("Exito AgregarProveedoresBL");
				}
				return var;
			} catch (Exception e) {
				System.out.println("Error AgregarProveedoresBL"+e.getMessage());
				throw e;
						
			}
		}
}
