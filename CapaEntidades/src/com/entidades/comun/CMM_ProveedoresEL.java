package com.entidades.comun;

import java.sql.Date;

public class CMM_ProveedoresEL {
	private int idProveedor;
	private String RazonSocial;
	private String Ruc;
	private String Direccion;
	private String Telefono1;
	private String email;
	private int AgenteRetenedor;
	private String CuentaContable;
	private boolean Activo;
	private Date FechaRegistro;

	public String getTelefono1() {
		return Telefono1;
	}

	public void setTelefono1(String telefono1) {
		Telefono1 = telefono1;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public int getAgenteRetenedor() {
		return AgenteRetenedor;
	}

	public void setAgenteRetenedor(int agenteRetenedor) {
		AgenteRetenedor = agenteRetenedor;
	}

	public String getCuentaContable() {
		return CuentaContable;
	}

	public void setCuentaContable(String cuentaContable) {
		CuentaContable = cuentaContable;
	}

	public boolean isActivo() {
		return Activo;
	}

	public void setActivo(boolean activo) {
		Activo = activo;
	}

	public Date getFechaRegistro() {
		return FechaRegistro;
	}

	public void setFechaRegistro(Date fechaRegistro) {
		FechaRegistro = fechaRegistro;
	}

	public int getIdProveedor() {
		return idProveedor;
	}

	public void setIdProveedor(int idProveedor) {
		this.idProveedor = idProveedor;
	}

	public String getRazonSocial() {
		return RazonSocial;
	}

	public void setRazonSocial(String razonSocial) {
		RazonSocial = razonSocial;
	}

	public String getRuc() {
		return Ruc;
	}

	public void setRuc(String ruc) {
		Ruc = ruc;
	}

	public String getDireccion() {
		return Direccion;
	}

	public void setDireccion(String direccion) {
		Direccion = direccion;
	}
}
