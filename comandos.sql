USE ERP_Comercial
GO
SELECT * FROM [Seguridad].[Usuario]
SELECT * FROM [Seguridad].[Menu]
SELECT * FROM [Seguridad].[MenuUsuario]
SELECT * FROM [Comun].[Persona]

PRINT [dbo].[fnDesencriptarClave](0x010000007DEBD5AEBD7AD6AD73469117A43E08EFE413DEE43B305A09)

ALTER PROCEDURE [dbo].[spSEG_VerificarAcceso]
@prmstrUsuario varchar(20),
@prmstrPassword varchar(20)
AS
SET NOCOUNT ON

	SELECT P.idPersona, P.Nombres, P.Apellidos, P.DNI, 
		P.FechaNacimiento, P.Sexo, U.idUsuario, U.Login, U.Activo
	FROM [Comun].Persona P (NOLOCK)
		INNER JOIN [Seguridad].Usuario U (NOLOCK) ON P.idPersona = U.idPersona
	WHERE U.Login = @prmstrUsuario AND 
		dbo.fnDesencriptarClave(U.Password) = @prmstrPassword
GO
CREATE PROCEDURE spSEG_ControlPermisosFecha
@prmintID int
AS
	SELECT u.idUsuario
	FROM Seguridad.Usuario u
	WHERE u.idUsuario = @prmintID AND u.PermisoHasta <= GETDATE()
GO

CREATE PROCEDURE spCMM_ListarProveedores
AS
	SELECT p.idProveedor, p.RazonSocial, p.Ruc, p.Direccion
	FROM [Logistica].[Proveedor] P (NOLOCK)
	ORDER BY P.RazonSocial
GO
SELECT * FROM [Logistica].[Proveedor]
GO
INSERT INTO [Logistica].[Proveedor]( RazonSocial, Ruc, Direccion,
	Telefono1, email,AgenteRetenedor, CuentaContable , Activo, FechaRegistro)
VALUES ('PRUEBA', '12345678910', 'ALGUN LUGAR X', '123456789', 'user@examen.com',1, '4221103' , 1, GETDATE())
GO
CREATE PROCEDURE spCMM_InsertarProveedores
	@prmstrRazonSocial nvarchar(100),
	@prmstrRuc varchar(11),
	@prmstrDireccion nvarchar(100),
	@prmstrTelefono1 nvarchar(15),
	@prmstremail nvarchar(50),
	@prmstrCuentaContable nvarchar(10)
AS
	INSERT INTO [Logistica].[Proveedor]( RazonSocial, Ruc, Direccion,
		Telefono1, email,AgenteRetenedor, CuentaContable , Activo, FechaRegistro)
	VALUES(@prmstrRazonSocial,@prmstrRuc,@prmstrDireccion,@prmstrTelefono1,
		@prmstremail,1,@prmstrCuentaContable,1,GETDATE())
GO