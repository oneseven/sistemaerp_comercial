package com.datos.comun;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.util.ArrayList;

import com.datos.seguridad.SEG_UsuarioDL;
import com.entidades.comun.CMM_PersonaEL;
import com.entidades.comun.CMM_ProveedoresEL;
import com.entidades.seguridad.SEG_UsuarioEL;

public class SEG_ProveedoresDL {
	// Singleton
	public static SEG_ProveedoresDL _Instancia;

	private SEG_ProveedoresDL() {
	};

	public static SEG_ProveedoresDL Instancia() {
		if (_Instancia == null) {
			_Instancia = new SEG_ProveedoresDL();
		}
		return _Instancia;
	}

	// endSingleton
	public ArrayList<CMM_ProveedoresEL> ListarProveedores() throws Exception {
		Connection cn = null;
		ArrayList<CMM_ProveedoresEL> lista = new ArrayList<CMM_ProveedoresEL>();
		try {
			cn = Conexion.Instancia().Conectar();
			CallableStatement cst = cn.prepareCall("{call spListarProveedores()}");
			ResultSet rs = cst.executeQuery();
			while (rs.next()) {
				CMM_ProveedoresEL p = new CMM_ProveedoresEL();
				p.setIdProveedor(rs.getInt("IdProveedor"));
				p.setRazonSocial(rs.getString("RazonSocial"));
				p.setRuc(rs.getString("Ruc"));
				p.setDireccion(rs.getString("Direccion"));
				lista.add(p);
			}
		} catch (Exception e) {
			throw e;
		} finally {
			cn.close();
		}
		return lista;
	}
	public boolean AgregarProveedores(CMM_ProveedoresEL proveedores) throws Exception {
		Connection con = null;
		CallableStatement cstm = null;
		try {
			con = Conexion.Instancia().Conectar();
			String sql = "{CALL spCMM_InsertarProveedores(?,?,?,?,?,?)}";
			cstm = con.prepareCall(sql);
			cstm.setString(1, proveedores.getRazonSocial());
			cstm.setString(2, proveedores.getRuc());
			cstm.setString(3, proveedores.getDireccion());
			cstm.setString(4, proveedores.getTelefono1());
			cstm.setString(5, proveedores.getEmail());
			cstm.setString(6, proveedores.getCuentaContable());
			cstm.execute();
			System.out.println("Exito consulta AgregarProveedores");
			return true;
		} catch (Exception e) {
			System.out.println("Error consulta AgregarProveedores");
			e.printStackTrace();
			return false;
		} finally {
			cstm.close();
			con.close();
		}
	}
}
