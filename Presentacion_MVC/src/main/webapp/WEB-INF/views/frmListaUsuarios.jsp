<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page session="false" %>
<html>
<head>
	<title>Home</title>
</head>
<body>

	<table border="1">
		<thead>
			<th>id</th>
			<th>Nombre</th>
			<th>Apellido</th>
			<th>Login</th>
		</thead>
		<tbody>
			<c:forEach items="${listita}" var="list">
				<tr>
					<td><c:out value="${list.idUsuario}"/></td>
					<td><c:out value="${list.persona.nombres}"/></td>
					<td><c:out value="${list.persona.apellidos}"/></td>
					<td><c:out value="${list.login}"/></td>
				</tr>
			</c:forEach>
		</tbody>
	</table>

</body>
</html>