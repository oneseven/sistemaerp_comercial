<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ page session="false"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Nuevo Proveedor</title>
</head>
<body>
	<form:form method="POST" action="NuevoProveedor">
		<table>
			<tr>
				<td>RazonSocial:</td>
				<td><input name="RazonSocial" type="text"/>
			<tr />
			<tr>
				<td>Ruc:</td>
				<td><input name="Ruc" type="text"/>
			<tr />
			<tr>
				<td>Direccion:</td>
				<td><input name="Direccion" type="text"/>
			<tr />
			<tr>
				<td>Telefono:</td>
				<td><input name="Telefono1" type="text"/>
			<tr />
			<tr>
				<td>Email:</td>
				<td><input name="Email" type="text"/>
			<tr />
			<tr>
				<td>CuentaContable:</td>
				<td><input name="CuentaContable" type="text"/>
			<tr />
			<tr>
				<td colspan="2">
					<input type="submit" value="Registrar" name="btnRegistrar" />
					<input type="reset" value="Limpiar" />
				</td>
			</tr>
		</table>
	</form:form>
</body>
</html>