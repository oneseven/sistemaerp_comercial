package com.presentacion.mvc;

import java.text.DateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;



import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.entidades.comun.CMM_ProveedoresEL;
import com.entidades.seguridad.SEG_UsuarioEL;
import com.logica.seguridad.CMM_ProveedoresBL;
import com.logica.seguridad.SEG_UsuarioBL;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@Controller
public class HomeController {
	
	private static final Logger logger = LoggerFactory.getLogger(HomeController.class);
	
	@RequestMapping(value = "/", method = RequestMethod.GET)
	public ModelAndView home() {
		return new ModelAndView("home", "command", new SEG_UsuarioEL());
	}

	@RequestMapping(value = "/VerificarAcceso", method = RequestMethod.POST)
	public String VerificarAcceso(@ModelAttribute("SpringWeb")SEG_UsuarioEL u, 
								ModelMap model){
		try {
			String Usuario = u.getLogin();
			String Password = u.getPassword();			
			SEG_UsuarioEL usu = SEG_UsuarioBL.Instancia().VerificarAcceso(
																Usuario, Password);
			return "frmMenuPrincipal";													
		} catch (Exception e) {
			System.out.println("Mensaje Valdiacion= "+e.getMessage());
			return "frmError";
		}
	}

	
	@RequestMapping(value = "/RegistroUsuario", method = RequestMethod.GET)
	public ModelAndView RegistroUsuario(HttpServletRequest request, 
								HttpServletResponse response){
		try {
			ArrayList<SEG_UsuarioEL> lista = 
					SEG_UsuarioBL.Instancia().ListarUsuarios();

			ModelAndView mav = new ModelAndView("frmListaUsuarios");
			mav.addObject("listita", lista);
			return mav;
			
		} catch (Exception e) {
			return new ModelAndView("home", "command", new SEG_UsuarioEL());
		}
	}
	@RequestMapping(value = "/RegistrarProveedores", method = RequestMethod.GET)
	public ModelAndView RegistrarProveedores(HttpServletRequest request, HttpServletResponse response) {
		try {
			ArrayList<CMM_ProveedoresEL> lista = CMM_ProveedoresBL.Instancia().ListarProveedores();

			ModelAndView mav = new ModelAndView("frmListarProveedores");
			mav.addObject("listas", lista);
			return mav;

		} catch (Exception e) {
			return new ModelAndView("home", "command", new CMM_ProveedoresEL());
		}
	}
	
	@RequestMapping(value = "/NuevoProveedor", method = RequestMethod.GET)
	public String NuevoProveedor(HttpServletRequest request, HttpServletResponse response) {
		try {
			return "frmNuevoProveedor";

		} catch (Exception e) {
			return "frmError";
		}
	}
	@RequestMapping(value = "/NuevoProveedor", method = RequestMethod.POST)
	public ModelAndView NuevoProveedor(@ModelAttribute("SpringWeb")CMM_ProveedoresEL p, ModelMap model) {
		try {
			CMM_ProveedoresEL prove = new CMM_ProveedoresEL();
			prove.setRazonSocial(p.getRazonSocial());
			prove.setRuc(p.getRuc());
			prove.setDireccion(p.getDireccion());
			prove.setTelefono1(p.getTelefono1());
			prove.setEmail(p.getEmail());
			prove.setCuentaContable(p.getCuentaContable());
			boolean var = CMM_ProveedoresBL.Instancia().AgregarProveedores(prove);
			
			ArrayList<CMM_ProveedoresEL> lista = CMM_ProveedoresBL.Instancia().ListarProveedores();
			ModelAndView mav = new ModelAndView("frmListarProveedores");
			mav.addObject("listas", lista);
			return mav;

		} catch (Exception e) {
			return new ModelAndView("home", "command", new CMM_ProveedoresEL());
		}
	}
}
